/********************************************************************

	Created		:	2017/04/03

	Filename	: 	bsp_uart.c

	Author		:	
	
	Description	:	

	History		:	
					
*********************************************************************/
/* Includes ------------------------------------------------------------------*/
#include "stm32f1xx_hal.h"
#include <stdarg.h>

#include "FreeRTOS.h"
#include "queue.h"
#include "portmacro.h"
#include "cmsis_os.h"
#include "string.h"

#define LOGD					bsp_uart_printf

#define MSG(fmt, args...)     	LOGD("MSG "fmt"", ## args)
#define LOG(fmt, args...)     	LOGD("LOG "fmt"", ## args)
#define ERR(fmt, args...)     	LOGD("ERR "fmt"", ## args)

#define FALSE	0
#define	TRUE	1

#define MIN(a, b) (((a) < (b)) ? (a) : (b))
#define MAX(a, b) (((a) > (b)) ? (a) : (b))

#define ADJ_MAX_COUNT	7

typedef struct TU88_S
{
	uint8_t monFlag;
	uint8_t adjFlag;
	uint8_t count;

	int64_t sensAvg0;
	int64_t sensAvg1;
	int64_t sensAvg2;
	int64_t sensAvg3;

	int64_t tempAvg0;
	int64_t tempAvg1;
	int64_t tempAvg2;
	int64_t tempAvg3;
	
	int32_t sensor[4];
	int32_t temp[4];
	int32_t avgSensor;
	int32_t avgTemp;

	float obj[4];
	int32_t sensorTh;
}TU88_ST;

int8_t send_que_default(uint32_t msg, uint32_t timeout);
void bsp_uart_printf(char *pcString, ...);
uint16_t bsp_uart_gets(uint8_t* buf);
void bsp_uart_Init(void);

void bsp_i2c_init(void);
int8_t bsp_i2c_write(uint16_t addr , uint8_t* buff, uint8_t size);
int8_t bsp_i2c_read(uint16_t addr , uint8_t* buff, uint8_t size);

void bsp_tu88_init(void);
int8_t bsp_tu88_read(uint32_t *,uint32_t *);

void bsp_led_green_tgl(void);

void swap(uint32_t *a, uint32_t *b);
int32_t median(int32_t a[], int n);
int32_t mean(int32_t a[], int n);
