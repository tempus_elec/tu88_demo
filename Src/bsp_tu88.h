/*
 * bsp_tu88.h
 *
 *  Created on: 2018. 1. 25.
 *      Author: Louie Yang
 */

#ifndef BSP_TU88_H_
#define BSP_TU88_H_

#define	TEMPUS_BUF_SIZE	12
#define TEMPUS_DEVICE_MAX_NUM	4

#define Start_NOM              	0xA8
#define Start_CM                0xA9

#define CMD_MEASURE				0xAA

#endif /* BSP_TU88_H_ */
