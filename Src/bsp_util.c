/*
 * bsp_util.c
 *
 *  Created on: 2018. 1. 30.
 *      Author: Louie Yang
 */


#include <stdint.h>

void swap(uint32_t *a, uint32_t *b)
{
	uint32_t tmp = *a;
	*a = *b;
	*b = tmp;
}

int32_t median(int32_t a[], int n) /* using Bubble sort */
{
	int i, j;

	for(i=0; i<n-1; ++i)
	{
		for(j=n-1; i<j; --j)
		{
			if(a[j-1] > a[j])
				swap(&a[j-1], &a[j]);
		}
	}

	return a[n/2];
}

int32_t mean(int32_t a[], int n)
{
	int32_t ret = 0;

	for(int i = 0; i < n; i++)
	{
		ret += a[i];
	}

	return (ret/n);
}


