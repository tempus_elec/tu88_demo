/********************************************************************

	Created		:	2017/04/03

	Filename	: 	cli.c

	Author		:	
	
	Description	:	

	History		:	
					
*********************************************************************/

#include <string.h>
#include <stdio.h>
#include <stdarg.h>
#include <stdint.h>
#include "cli.h"
/* Includes ------------------------------------------------------------------*/
#include "stm32f1xx_hal.h"
#include "cmsis_os.h"
#include "bsp.h"

#define CLI_TASK_PRIO			osPriorityLow

#define CLI_TASK_STACK			128

osThreadId hCliTask;

static CliJte *cliFindCommand(char *, CliJte *);
static Bool    cliGetStringAndTokenise(char *, CliJte *);
static Bool    cliParseTokens(CliJte *, char **, int);
static void    cliHelp(CliJte *);
static size_t dbgGetString(char *pbLine, unsigned int cbLine);
//static Bool cliCommandInterpreter (int command, int argc, char** argv);

extern void cliCommandsInitialise (void);

//bool cliEnabled = FALSE; // Global flag to indicate whether or not the cli is active

/********************************************************************/
/*                    Local Constants                             */
/********************************************************************/
#define CLI_MAX_COMMANDS                     (32)
#define CLI_MAX_COMMAND_LINE                 (64)
#define CLI_MAX_TOKENS                       (16)

/********************************************************************/
/*                    Internally Visible Static Data              */
/********************************************************************/
static char  *cliWhiteSpace=" \"\n\r\t\0";
static char  *cliWhiteNoSpace="\"\n\r\t";
static CliJte cliRootTable[CLI_MAX_COMMANDS];
static CliJte cliRootNode = { "", "", "", 0, 0, cliRootTable, 0, NULL};
static Bool cliQuit = FALSE;

/********************************************************************/
/*                   					            */
/********************************************************************/
int appQuit = 0;

// Define the prompts and table types.
char* pPromptCli = {"CLI"};
char* pTableCli = {"CLI"};

// Current table and prompt that are in use
char* pTableInUse;
char* pPromptInUse;

static char* pProgramName = "Command Line Interface By TEMPUS";

void StartCliTask(void const * argument)
{
	/* Create the thread(s) */
	/* definition and creation of defaultTask */
	osThreadDef(cliTask, cliTask, CLI_TASK_PRIO, 0, CLI_TASK_STACK);
	hCliTask = osThreadCreate(osThread(cliTask), NULL);
}
/****************************************************************************
* PARAMETERS:      Name          Name under which to register table.
*                  Help          String for user to identify table.
*                  NumberInTable How many entries.
*                  JTEtable      Pointer to table structure to register.
*
* DESCRIPTION:     Register a command table with the CLI.
***************************************************************************/
/*****************************************************************************
* Example:
*                                                                           
* JTE ALAtable[]={ { "set","mjd; h, m, s", "Set alarm", NULL, 0, NULL, 5, ALAcommandInterpreter},
*                  { "get","id",           "Get alarm", NULL, 0, NULL, 0, ALAcommandInterpreter} } ;
*
* JTE TIMtable[]={ {"set",   "mjd; h, m, s", "Set time", TIM_TEST_TIME_SET, 5, NULL,     0, TIMcommandInterpreter},
*                  {"get",   "id",           "Get time", TIM_TEST_TIME_GET, 1, NULL,     0, TIMcommandInterpreter},
*                  {"alarm", "",             "Alarms",   NULL,              0, ALAtable, 2, NULL} };
*
* Result = CLIregisterTable("TIM", "TIM help", 3, TIMtable);
*
*****************************************************************************/
void cliRegisterTable(char *name, char *help, int numberInTable, CliJte *table)
{
    CliJte *newCommand;

    if (cliRootNode.numberInNextLevel >= CLI_MAX_COMMANDS)
    {
        CLI_LOG( "Out of space in command table.\n" );
        return;
    }

    if (cliFindCommand(name, &cliRootNode) != NULL)
    {
		CLI_LOG("Table '%s' registered twice.\n", name);
        return;
    }

    newCommand = &(cliRootNode.nextLevel[cliRootNode.numberInNextLevel++]);

    newCommand->name              = name;
    newCommand->help              = help;
    newCommand->args              = NULL;
    newCommand->argCount          = 0;
    newCommand->userInt           = 0;
    newCommand->numberInNextLevel = numberInTable;
    newCommand->nextLevel         = table;
    newCommand->callback          = NULL;
}


/********************************************************************/
/***                        Static functions                        */
/********************************************************************/

/****************************************************************************
* DESCRIPTION:    Command line interpreter task
****************************************************************************/
void cliTask(void const * argument)
{
    Bool bLoopFlag1;
    CliJte* pNode;

    // Initialize the globals
	pTableInUse = pTableCli;
	pPromptInUse = pPromptCli;
	
	// Initialize BSP_Uart_Init(), actual UART_Init will be call from main function.
	//bsp_uart_Init();
	// Initialize all of our commands
	cliCommandsInitialise();
	
	// Shift screen
	CLI_PRINT("\033[2J\033[H\n");
	CLI_PRINT("%s started\n", pProgramName);

    // Go direct to the command line
    bLoopFlag1 = TRUE;
    while (bLoopFlag1 == TRUE)
    {
        pNode = cliFindCommand (pTableInUse, &cliRootNode);
        if (pNode != NULL)
        {
            cliGetStringAndTokenise (pPromptInUse, pNode);
        }

        if (cliQuit == TRUE)
        {
            // We have been told to quit
            bLoopFlag1 = FALSE;
        }
    }
}

/***************************************************************************
* PARAMETERS:     String: String to print.
*
* RETURN VALUES:  TRUE if an EXIT ('.') command was executed
*
* DESCRIPTION:    Get input from DBG and convert to tokens
****************************************************************************/
static Bool cliGetStringAndTokenise( char *string, CliJte *node )
{
    static char  line[CLI_MAX_COMMAND_LINE + 1];
    static char *tokens[CLI_MAX_TOKENS];
    static int   tokenCount;
    Bool         inQuotes = FALSE; 

    /* Ask for string and Wait. */
    CLI_LOG( "%s> ", string );
    //printf( "> " );

    /* Fix for bug 1305 */
    memset(line, 0x00, sizeof(line));

    dbgGetString(line, sizeof(line));
    if (cliQuit)
    {
        return TRUE;
    }
    CLI_PRINT( "\n" );

    /* Parse string. */
    if ( line[0]=='\n' || line[0]=='\r' ) return FALSE;
    if ( line[0]=='.' ) return TRUE;

    /* Single help. */
    if ( line[0] == '?')
    {
        cliHelp(node);
        return FALSE;
    }

    /* Tokenise the line. */
    tokenCount = 0;
    tokens[0]  = strtok(line, cliWhiteSpace);
    while( tokens[tokenCount] )
    {
        if ( tokenCount++ == CLI_MAX_TOKENS )
        {
            tokenCount=0;
            break;
        }

        if (*(tokens[tokenCount-1]+strlen(tokens[tokenCount-1])+1) == '"')
        {
            inQuotes = TRUE; 
        }
        else
        {
            inQuotes = FALSE; 
        }

        if (inQuotes)
        {
            /* Uses the last known string. Dodgy stuff. */
            tokens[tokenCount] = strtok(NULL, cliWhiteNoSpace);
        }
        else
        {
            /* Uses the last known string. Dodgy stuff. */
            tokens[tokenCount] = strtok(NULL, cliWhiteSpace);
        }
    }

    if ( tokenCount > 0 )
    {
        if (!cliParseTokens(node, tokens, tokenCount))
        {
            CLI_PRINT( "Unrecognised command, try '?' for help\n" );
        }
    }

    return FALSE;
}


/****************************************************************************
* PARAMETERS:     table           Pointer to the JTE node to parse
*                 tokens          Strings to parse.
*                 tokenCount      Number of tokens
*
* RETURN VALUES:  Success
*
* DESCRIPTION:    Parses an array of tokens against a JTE table structure.
*                 Fails, moves to a sub table or executes a command.
*                 Prints help on command failure.
***************************************************************************/
static Bool cliParseTokens(CliJte *jte, char **tokens, int tokenCount)
{
    Bool    result = FALSE;
    CliJte *command;

    /* Is there a match in the current JTE table. */
    if ((command = cliFindCommand(tokens[0], jte)) != NULL)
    {
        /* Is the command a table. */
        if (command->numberInNextLevel > 0)
        {
            /* Table. Are there more tokens to use. */
            if (tokenCount == 1)
            {
                /* No sub-tokens. All done. Open a new command shell. */
                CLI_PRINT( "Type '.' to exit, or '?' for local help\n" );
                while ( !cliGetStringAndTokenise(command->name, command) );
                result = TRUE;
            }
            else
            {
                /* Remove Command and Parse remaining tokens. */
                result = cliParseTokens(command, tokens + 1, tokenCount - 1);
            }

        }
        else if (command->callback != NULL)
        {
            /* Function. Check the number of tokens/arguments against the function. */
            if ((command->argCount < 0) && (tokenCount < -command->argCount))
            {
                CLI_PRINT( "Not enough arguments; at least %d required.\n", (-command->argCount)-1 );
            }
            else if ((command->argCount > 0) && (tokenCount != command->argCount))
            {
                CLI_PRINT( "Incorrect number of arguments.\n" );
            }
            else
            {
                /* Call callback. */
                result = (*command->callback)(command->userInt, tokenCount, tokens);
            }
        }
        
        /* If failed to execute given command. */
        if (result == FALSE)
        {
            cliHelp(command);
            result = TRUE;
        }
    }

    return result;
}


/*************************************************************************
* PARAMETERS:     name         command name
*                 jte          jte to look in
*
* RETURN VALUES:  Returns a pointer to the JTE entry that has been found to match.
*
* DESCRIPTION:    Find the given command with the table
***************************************************************************/
static CliJte *cliFindCommand(char *name, CliJte *jte)
{
    CliJte       *command;
    unsigned int  i, index;
    size_t        length = strlen(name);

    /* For all entries in table, get command and try to match. */
    for (i = 0; i < jte->numberInNextLevel; i++)
    {
        command = &jte->nextLevel[i];
        for (index = 0; index < length; index++)
        {
            /* Ignore Capitals. */
            if (tolower(command->name[index]) != tolower(name[index]))
            {
                break;
            }
        }

        /* Did we get to the end of name. */
        if (command->name[index] == '\0')
        {
            if (index == length)
            {
                return command;
            }
        }
    }

    return NULL;
}

/****************************************************************************
* PARAMETERS:     Pointer to JTE structure
*
* DESCRIPTION:    Print help for the command or table
***************************************************************************/
static void cliHelp (CliJte* jte)
{
    unsigned int i;

    // If the supplied pointer is the root node of the help
    // don't print anything
    if (jte != &cliRootNode)
    {
        {
            CLI_LOG ("%14s", jte->name);
        }

        if (jte->args != 0)
        {
            {
                CLI_LOG ("[%s]\n\r%14s", jte->args, "");
            }
        }

        if (jte->help == 0)
        {
            // No help.
            CLI_LOG( "no help\n" );
        }
        else
        {
            CLI_PRINT( "%s\n", jte->help );
        }
    }

    /* If this is a table then print out help for the entries. */
    if (jte->numberInNextLevel > 0)
    {
        for(i=0; i < jte->numberInNextLevel; i++)
        {
            cliHelp(&jte->nextLevel[i]);
        }
        CLI_LOG ("\n");
    }
}


size_t dbgGetString(char *pbLine, unsigned int cbLine) 
{
	if (bsp_uart_gets((uint8_t*)pbLine))
	{
	    return (strlen(pbLine));
    }
    return 0;
}

void cliExit(void)
{
    cliQuit = TRUE;
}



