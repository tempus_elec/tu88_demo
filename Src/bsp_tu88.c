/*
 * bsp_tu88.c
 *
 *  Created on: 2018. 1. 25.
 *      Author: Louie Yang
 */
#include "bsp.h"
#include "bsp_tu88.h"
#include "adc.h"
#include "dma.h"

#define TU88_AVG_COUNT		8

uint8_t i2c_buf[TEMPUS_BUF_SIZE] = {0};
uint8_t tempus_addr[] = { 0x00, 0x02, 0x04, 0x06 };

static int32_t tu88_sensor[TEMPUS_DEVICE_MAX_NUM] = {0};
static int32_t tu88_temp[TEMPUS_DEVICE_MAX_NUM] = {0};

uint8_t cmdBuf[11][3] = {
								{0x12 + 0x40, 0x8e, 0x3e},
								{0x09 + 0x40, 0x00, 0x00},
								{0x11 + 0x40, 0x40, 0x01},
								{0x0B + 0x40, 0x41, 0x52},
								{0x0C + 0x40, 0x45, 0x9b},
								{0x10 + 0x40, 0x00, 0x0a},
								{0x0A + 0x40, 0x39, 0x30},
								{0x0D + 0x40, 0x01, 0x10},
								{0x04 + 0x40, 0x00, 0x00},
								{0x17 + 0x40, 0x00, 0x00},
								{0x19 + 0x40, 0x00, 0x00}
						};

uint32_t adcBuff[4] = {0};

uint8_t avgParam = TU88_AVG_COUNT;

extern TU88_ST g_tu88;
extern ADC_HandleTypeDef hadc1;
extern DMA_HandleTypeDef hdma_adc1;

extern int32_t tu88_0_offset;
extern int32_t tu88_1_offset;
extern int32_t tu88_2_offset;
extern int32_t tu88_3_offset;
extern uint32_t SENS_TH;
/*---------------------------------------------------------------------------*/
static int8_t tu88_read(uint8_t addr, uint32_t* sensor, uint32_t* temp);
static void check_busy(uint16_t addr);
static int8_t tu88_ad_read(void);
int8_t tu88_adj(void);
int8_t handle_led(uint32_t* sens, uint32_t* temp);
/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
void bsp_tu88_init(void)
{
	g_tu88.monFlag = 1;

	i2c_buf[0] = Start_CM;
	i2c_buf[1] = 0;
	i2c_buf[2] = 0;

	for(int i = 0; i < TEMPUS_DEVICE_MAX_NUM; i++)
	{
		bsp_i2c_write(tempus_addr[i], i2c_buf, 3);
		osDelay(10);
	}

#if 1	// ROIC default setting
	for(int j = 0; j < 11; j++)
	{
		for(int i = 0; i < TEMPUS_DEVICE_MAX_NUM; i++)
		{
			bsp_i2c_write(tempus_addr[i], &cmdBuf[j][0], 3);
			//LOG("Write Reg : 0x%x, 0x%x, 0x%x\n", cmdBuf[j][0], cmdBuf[j][1], cmdBuf[j][2]);
			osDelay(10);
		}
	}
#endif

	LOG("Tempus Init Done\n");
}


int8_t bsp_tu88_read(uint32_t *tu88_sensor,uint32_t *tu88_temp)
{
	if(g_tu88.adjFlag)
	{
		tu88_adj();
		g_tu88.adjFlag = 0;
	}
	else
	{
		for(int i = 0; i < TEMPUS_DEVICE_MAX_NUM; i++)
		{
			tu88_read(tempus_addr[i], &tu88_sensor[i], &tu88_temp[i]);
			osDelay(17);
			//LOG("%d %ld %ld\n", tempus_addr[i], tu88_sensor[i], tu88_temp[i]);
			g_tu88.obj[i] = (float)((((tu88_sensor[i]/16777216.0)-0.5)*1000.0) + ((tu88_temp[i]/16777216.0)*125.0 - 40.0));
		}
		//MSG("ROIC %ld %ld %ld %ld %ld %ld %ld %ld\n", tu88_sensor[0] + tu88_0_offset, tu88_temp[0], tu88_sensor[1] + tu88_1_offset, tu88_temp[1], tu88_sensor[2] + tu88_2_offset, tu88_temp[2], tu88_sensor[3] + tu88_3_offset, tu88_temp[3]);	
		MSG("ROIC %ld %lf %ld %lf %ld %lf %ld %lf\n", tu88_sensor[0] + tu88_0_offset, g_tu88.obj[0], tu88_sensor[1] + tu88_1_offset, g_tu88.obj[1], tu88_sensor[2] + tu88_2_offset, g_tu88.obj[2], tu88_sensor[3] + tu88_3_offset, g_tu88.obj[3]);	
	}

	// check whether led on off
	handle_led(tu88_sensor, tu88_temp);
	//tu88_ad_read();

	return 0;
}

static int8_t tu88_read(uint8_t addr, uint32_t* sensor, uint32_t* temp)
{
	int ret = 0;

	i2c_buf[0] = CMD_MEASURE;
	i2c_buf[1] = 0;
	i2c_buf[2] = 0;
	ret = bsp_i2c_write(addr, i2c_buf, 3);
	check_busy(addr);
	ret = bsp_i2c_read(addr, i2c_buf, 7);
	*sensor=(i2c_buf[1]<<16 | i2c_buf[2]<<8 | i2c_buf[3]);
	*temp=(i2c_buf[4]<<16 | i2c_buf[5]<<8 | i2c_buf[6]);

/*	for(int i = 0; i < TU88_AVG_COUNT; i++)
	{
		ret = bsp_i2c_write(addr, i2c_buf, 3);
		check_busy(addr);
		ret = bsp_i2c_read(addr, i2c_buf, 7);
		sen=(i2c_buf[1]<<16 | i2c_buf[2]<<8 | i2c_buf[3]);
		tem=(i2c_buf[4]<<16 | i2c_buf[5]<<8 | i2c_buf[6]);
		if(i==0){
			max=sen;
			min=sen;
		};
		sum_sensor +=sen;
		sum_temp += tem;
		if(sen>max) max=sen;
		if(sen<min) min=sen;
//		osDelay(17);
	}
	*sensor =(sum_sensor);//-max-min(uint32_t)
	*temp = (sum_temp);//TU88_AVG_COUNT);(uint32_t)*/

	return ret;
}

static void check_busy(uint16_t addr)
{
    int i = 0;
    uint8_t busy = 0;

    while(1)
    {
    	bsp_i2c_read(addr, &busy, 1);

        if ((busy&0x20) ==0)
        {
            break;
        }

        osDelay(5);

        i++;
        if (i > 10)
        {
            LOG("Tempus busy check error!!!\n");
            break;
        }
    }
}

static int8_t tu88_ad_read(void)
{
	HAL_StatusTypeDef ret = HAL_OK;

	adcBuff[0] = adcBuff[1] = adcBuff[2] = adcBuff[3] = 0;

	ret = HAL_ADC_Start_DMA(&hadc1, adcBuff, 4);
	if(ret != HAL_OK)
	{
		ERR("%s error...%d\n", ret);
		return -1;
	}
	//LOG("AD %d %d %d %d\n", adcBuff[0], adcBuff[1], adcBuff[2], adcBuff[3]);
	return 0;
}

void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef* hadc)
{
	if(hadc == &hadc1)
	{
		MSG("AD %d %d %d %d\n", (int32_t)adcBuff[0], (int32_t)adcBuff[1], (int32_t)adcBuff[2], (int32_t)adcBuff[3]);
	}
	else
	{

	}
}

int8_t tu88_adj(void)
{
	// adjust tu88 0/1/2/3
	g_tu88.sensAvg0 = 0;
	g_tu88.sensAvg1 = 0;
	g_tu88.sensAvg2 = 0;
	g_tu88.sensAvg3 = 0;
	
	for(int j = 0; j < ADJ_MAX_COUNT; j++)
	{
		for(int i = 0; i < TEMPUS_DEVICE_MAX_NUM; i++)
		{
			tu88_read(tempus_addr[i], &g_tu88.sensor[i], &g_tu88.temp[i]);
			osDelay(20);
		}
		g_tu88.sensAvg0 += g_tu88.sensor[0];
		g_tu88.sensAvg1 += g_tu88.sensor[1];
		g_tu88.sensAvg2 += g_tu88.sensor[2];
		g_tu88.sensAvg3 += g_tu88.sensor[3];
	}
	g_tu88.sensAvg0 /= ADJ_MAX_COUNT;
	g_tu88.sensAvg1 /= ADJ_MAX_COUNT;
	g_tu88.sensAvg2 /= ADJ_MAX_COUNT;
	g_tu88.sensAvg3 /= ADJ_MAX_COUNT;
}


int8_t handle_led(uint32_t* sens, uint32_t* temp)
{
	if(		(sens[0] > (g_tu88.sensAvg0 + SENS_TH))	|| (sens[1] > (g_tu88.sensAvg1 + SENS_TH)) || (sens[2] > (g_tu88.sensAvg2 + SENS_TH)) || (sens[3] > (g_tu88.sensAvg3 + SENS_TH)) )
	{
		MSG("LED ON\n");
	}
	else
	{
		MSG("LED OFF\n");
	}
}


