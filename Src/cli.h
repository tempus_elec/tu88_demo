/********************************************************************

	Created		:	2017/04/03

	Filename	: 	cli.h

	Author		:	
	
	Description	:	

	History		:	
					
*********************************************************************/

#ifndef CLI_H
#define CLI_H

#include <stddef.h>
#include <ctype.h>
#include <stdlib.h>

#define SPAN_OF(x)      (sizeof (x) / (sizeof (x[0])))
#define INC_MOD(i, m)   (((i)>=((m)-1)) ? (0) : ((i)+1))

#define FALSE	0
#define TRUE	1

/* Make sure that the CLI can still work when DEBUG is off */
#define CLI_PRINT   bsp_uart_printf
#define CLI_LOG     bsp_uart_printf

// CLI commands that are not DAB or DVB
#define CLI_CMD_OFFSET          	        (0x100)
#define CLI_CMD_EXIT                        (CLI_CMD_OFFSET + 1)
#define CLI_CMD_INITIALISE      	        (CLI_CMD_OFFSET + 2)
#define CLI_CMD_UNINITIALISE    	        (CLI_CMD_OFFSET + 3)
#define CLI_CMD_BOOT                        (CLI_CMD_OFFSET + 4)
#define CLI_CMD_LOAD_INI_FILE               (CLI_CMD_OFFSET + 5)
#define CLI_CMD_LOG                         (CLI_CMD_OFFSET + 6)
#define CLI_CMD_DISPLAY                     (CLI_CMD_OFFSET + 7)
#define CLI_CMD_ECHO                        (CLI_CMD_OFFSET + 8)
#define CLI_CMD_MONITOR                     (CLI_CMD_OFFSET + 9)
#define CLI_CMD_AVG_COUNT		            (CLI_CMD_OFFSET + 10)
#define CLI_CMD_ROIC_WRITE		            (CLI_CMD_OFFSET + 11)
#define CLI_CMD_TEMPUS_TEMP_READ            (CLI_CMD_OFFSET + 12)
#define CLI_CMD_TU88_OFFSET            		(CLI_CMD_OFFSET + 13)
#define CLI_CMD_SENS_THRESHOLD          	(CLI_CMD_OFFSET + 14)
#define CLI_CMD_SHT3X_TEMP_READ		        (CLI_CMD_OFFSET + 15)
#define CLI_CMD_MLX90614_READ		        (CLI_CMD_OFFSET + 16)

#define CLI_CMD_MAX                         (CLI_CMD_OFFSET + 50)
///////////////////////////////////////////////

typedef signed char         int8;
typedef signed short        int16;
typedef signed long         int32;
typedef long long           int64;

typedef unsigned char       uint8;
typedef unsigned short      uint16;
typedef unsigned int       uint32;
typedef unsigned long long  uint64;

typedef unsigned char Bool;


typedef Bool CLI_CALLBACK (int, int, char **);

/* Jump table entries. */
typedef struct CliJte
{
    char *name;
    char *args;
    char *help;
    int   userInt;
    int      argCount;     /* -N means "at least N" */
    struct CliJte *nextLevel;
    unsigned int  numberInNextLevel;
    CLI_CALLBACK *callback;
} CliJte;


// Extern global variables.
extern char* pPromptCli;
extern char* pTableCli;

extern char* pTableInUse;
extern char* pPromptInUse;


/*****************************************************************************/
/*******                  Public Function Interface                     ******/
/*****************************************************************************/

void cliRegisterTable(char *, char *, int, CliJte *);
void cliInitialise(void);
void cliTask(void const * argument);
void cliExit(void);
void cliInitLogFile( void );

/* The following functions are for Logging output to a file. The primary reason
** for adding this functionality is so that WinBatch can read responces from
** the file. WinBatch cannot use stdout because the output does not get flushed
** until the application is closed.
*/


void cliCommandsInitialise (void);
void StartCliTask(void const * argument);

#endif

