/********************************************************************

	Created		:	2017/04/03

	Filename	: 	bsp_uart.c

	Author		:	
	
	Description	:	

	History		:	
					
*********************************************************************/
/* Includes ------------------------------------------------------------------*/
#include <stdarg.h>
#include "FreeRTOS.h"
#include "queue.h"
#include "portmacro.h"
#include "cmsis_os.h"
#include "string.h"
#include "bsp.h"

extern UART_HandleTypeDef huart2;

static osMessageQId uartQueHandle;

USART_TypeDef *		cliPort = USART2;
UART_HandleTypeDef* cliHandle = &huart2;
/*--------------------------------------------------------------------------*/
osMutexId uartMutexHandle;
osMutexId cliMutexHandle;
/*--------------------------------------------------------------------------*/
/*--------------------------------------------------------------------------*/
void bsp_uart_getCh(UART_HandleTypeDef *huart);
/*--------------------------------------------------------------------------*/


void bsp_uart_Init(void)
{
	/* definition and creation of buzzQue */
	osMessageQDef(cliQue, 16, uint16_t);
	uartQueHandle = osMessageCreate(osMessageQ(cliQue), NULL);

	osMutexDef(uartMutex);
	uartMutexHandle = osMutexCreate(osMutex(uartMutex));
}

void UARTwrite(char* str, uint32_t size)
{
	osMutexWait(uartMutexHandle, osWaitForever);

	HAL_UART_Transmit(cliHandle, (uint8_t*)str, size, 100);

	osMutexRelease(uartMutexHandle);
}

void HAL_printf_valist(const char *fmt, va_list argp)
{
	char string[128];
	vsprintf(string, fmt, argp);

    UARTwrite(string, strlen(string));
}

/** Custom printf function, only translate to va_list arg HAL_UART.
 * @param *fmt String to print
 * @param ... Data
 */
void bsp_uart_printf(char *pcString, ...)
{
	va_list argp;

	va_start(argp, pcString);
	HAL_printf_valist(pcString, argp);
	va_end(argp);
}

uint16_t bsp_uart_gets(uint8_t* buf)
{
	HAL_StatusTypeDef result;
	uint16_t size = 0;
	char ch;
	static uint8_t errCount = 0;
	while(1)
	{
		while (HAL_UART_GetState(cliHandle) != HAL_UART_STATE_READY)
		{
			errCount++;
			if(errCount > 10)
			{
				ERR("UART Busy ERROR\n");
				cliHandle->Instance->CR1 &= 0xFFFE;
				osDelay(10);
				cliHandle->Instance->CR1 |= 0x0001;
				osDelay(10);
				errCount = 0;
			}
		}
		
		result = HAL_UART_Receive_IT(cliHandle, (uint8_t*)&ch, 1);
		if(result != HAL_OK)
		{
			LOG("result - %d - %c\n", result, ch);
		}

		osEvent evt = osMessageGet (uartQueHandle, osWaitForever);

		if(evt.status == osEventMessage)
		{
			ch = evt.value.v;
			// no echo
			//bsp_uart_printf("%c", ch);
			// in case of line end, return
			if( (ch == '\n') || (ch == '\r') )
			{
				return size;
			}
			
			buf[size++] = ch;
			if(size > 64*2)
				size = 0;
		}
		else
		{
			
		}

		bsp_uart_printf("%c", ch);
	}
}

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
	if(huart->Instance == cliPort)
	{
		bsp_uart_getCh(huart);
	}
	else
	{

	}
}

void bsp_uart_getCh(UART_HandleTypeDef *huart)
{
	osMessagePut (uartQueHandle, huart->Instance->DR & (uint16_t)0x01FF, 10);
}

