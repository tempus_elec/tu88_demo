/*
 * bsp_led.c
 *
 *  Created on: 2018. 1. 26.
 *      Author: Louie Yang
 */
#include "bsp.h"
#include "gpio.h"


void bsp_led_green_tgl(void)
{
	HAL_GPIO_TogglePin(LED_GREEN_GPIO_Port, LED_GREEN_Pin);
}

