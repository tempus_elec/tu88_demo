/********************************************************************

	Created		:	2017/04/03

	Filename	: 	cli_command.c

	Author		:	
	
	Description	:	

	History		:	
					
*********************************************************************/
#include <string.h>
#include <stdio.h>
#include <stdarg.h>
#include <stdint.h>
#include <stdbool.h>
#include "cli.h"
#include "stm32f1xx_hal.h"
#include "cmsis_os.h"
#include "bsp.h"

static Bool cliCommandInterpreter (int command, int argc, char** argv);


/***********EXTERNS*********************/
extern osThreadId defaultTaskHandle;

extern osMessageQId cliQueHandle;

extern osSemaphoreId cliSemHandle;

extern TU88_ST g_tu88;
extern uint8_t tempus_addr[];
extern uint8_t avgParam;

extern int32_t tu88_0_offset;
extern int32_t tu88_1_offset;
extern int32_t tu88_2_offset;
extern int32_t tu88_3_offset;
extern bool LED_STATE;
extern uint32_t LED_OFF_STATE_COUNT;
extern uint32_t SENS_TH;//sensor threshold
/***********END OF EXTERNS***************/

float sen = 0.0;
float tem = 0.0;
float tobj = 0.0;

char log_buf[256] = {0};

/**************************** PRIVATE FUNCTION DEFINITIONS *******************/
static CliJte cliCommandTable [] =
{
	{ 	"init",
		NULL,
		"Initialize",
		CLI_CMD_INITIALISE,
		1,
		NULL,
		0,
		&cliCommandInterpreter
	},
	{ 	"deinit",
		NULL,
		"De-initialize",
		CLI_CMD_UNINITIALISE,
		1,
		NULL,
		0,
		&cliCommandInterpreter
	},
	{ 	"mon",
		NULL,
		"enable monitor",
		CLI_CMD_MONITOR,
		2,
		NULL,
		0,
		&cliCommandInterpreter
	},
	{ 	"roic",
		NULL,
		"ROIC Register Write",
		CLI_CMD_ROIC_WRITE,
		5,
		NULL,
		0,
		&cliCommandInterpreter
	},
	{ 	"r",
		NULL,
		"Read TEMPUS temp sensor",
		CLI_CMD_TEMPUS_TEMP_READ,
		1,
		NULL,
		0,
		&cliCommandInterpreter
	},
	{ 	"avgcount",
		NULL,
		"Set average counter param",
		CLI_CMD_AVG_COUNT,
		2,
		NULL,
		0,
		&cliCommandInterpreter
	},
	{ 	"tu88_offset",
		NULL,
		"Set TU88 offset values",
		CLI_CMD_TU88_OFFSET,
		5,
		NULL,
		0,
		&cliCommandInterpreter
	},
	{ 	"sens_th",
		NULL,
		"Set Sensor Threshold Value",
		CLI_CMD_SENS_THRESHOLD,
		2,
		NULL,
		0,
		&cliCommandInterpreter
	},
	{ 	"exit",
		NULL,
		"Quits the application",
		CLI_CMD_EXIT,
		1,
		NULL,
		0,
		&cliCommandInterpreter
	},
};

void cliCommandsInitialise (void)
{
	cliRegisterTable (pTableCli,
					"CLI commands",
					sizeof (cliCommandTable) / sizeof (cliCommandTable [0]),
					cliCommandTable
	);
}

static Bool cliCommandInterpreter (int command, int argc, char** argv)
{
	Bool result = TRUE; // Assume that we are going to process the command
	uint8_t val;
	uint8_t roicBuf[3] = {0};
	uint8_t mode = 0;
	
	switch (command)
	{
		case CLI_CMD_INITIALISE:

			break;

		case CLI_CMD_UNINITIALISE:

			break;

        case CLI_CMD_MONITOR:
        	val = atoi(argv[1]);
            g_tu88.monFlag = val;
            LOG("mon set to %d\n", val);
            break;

        case CLI_CMD_TEMPUS_TEMP_READ:

        	break;

        case CLI_CMD_ROIC_WRITE:
        	val = atoi(argv[1]);		// TU88 addr
        	switch(val)
        	{
				case 0: val = 0x00;	mode = 1;
					break;
				case 1: val = 0x02;	mode = 1;
					break;
				case 2: val = 0x04;	mode = 1;
					break;
				case 3: val = 0x06;	mode = 1;
					break;
				default: 	mode = 0;
					break;
        	}

        	if(mode)
        	{
        		roicBuf[0] = atoi(argv[2])  + 0x40;
				roicBuf[1] = atoi(argv[3]);
				roicBuf[2] = atoi(argv[4]);
				bsp_i2c_write(val, &roicBuf[0], 3);
        	}
        	else
        	{
        		roicBuf[0] = atoi(argv[2])  + 0x40;
				roicBuf[1] = atoi(argv[3]);
				roicBuf[2] = atoi(argv[4]);
        		for(int i = 0; i < 4; i++)
        		{
        			bsp_i2c_write(tempus_addr[i], &roicBuf[0], 3);
					osDelay(50);
        		}
        	}
			osDelay(100);
        	ERR("ROIC %d 0x%x 0x%x 0x%x\n", val, roicBuf[0], roicBuf[1], roicBuf[2]);
        	break;

		case CLI_CMD_AVG_COUNT:
			avgParam = atoi(argv[1]);
			ERR("AVG Count set to %d\n", avgParam);
			break;

		case CLI_CMD_TU88_OFFSET:
			tu88_0_offset = atoi(argv[1]);
			tu88_1_offset = atoi(argv[2]);
			tu88_2_offset = atoi(argv[3]);
			tu88_3_offset = atoi(argv[4]);
			LED_STATE = LED_OFF_STATE_COUNT = 0;

			LOG("TU88 Offset set to %d, %d, %d, %d\n", tu88_0_offset, tu88_1_offset, tu88_2_offset, tu88_3_offset);
			break;

		case CLI_CMD_SENS_THRESHOLD:
			SENS_TH = atoi(argv[1]);
			g_tu88.adjFlag = 1;
			LOG("SENS_TH set to %d\n", SENS_TH);
			break;
			
		case CLI_CMD_EXIT:
		    // Quit the program
		    cliExit ();
		    break;

		default:
		    // Unknown command! We should never get here...
		    CLI_LOG ("Unknown command, %d\n", command);
		    result = FALSE;
		    break;
	}

	return result;
}

