/*
 * bsp_i2c.c
 *
 *  Created on: 2018. 1. 25.
 *      Author: Louie Yang
 */
#include <stdarg.h>
#include "FreeRTOS.h"
#include "queue.h"
#include "portmacro.h"
#include "cmsis_os.h"
#include "stm32f1xx_hal.h"
#include "i2c.h"
#include "bsp.h"

extern I2C_HandleTypeDef hi2c1;
osMutexId i2cMutexHandle;

void bsp_i2c_init(void)
{
	osMutexDef(i2cMutex);
	i2cMutexHandle = osMutexCreate(osMutex(i2cMutex));
}

int8_t bsp_i2c_write(uint16_t addr , uint8_t* buff, uint8_t size)
{
	HAL_StatusTypeDef ret = HAL_OK;

	osMutexWait(i2cMutexHandle, osWaitForever);

	ret = HAL_I2C_Master_Transmit(&hi2c1, addr, buff, size, 100);	
	if(ret != HAL_OK)
	{
		ERR("[%s] error...%d\n", __func__, ret);

		ret = HAL_I2C_DeInit(&hi2c1);
		if(ret != HAL_OK)
		{
			ERR("HAL_I2C_DeInit error...%d\n", ret);
		}

		MX_I2C1_Init();

		ERR("i2c re-initialized\n");
		osMutexRelease(i2cMutexHandle);
		return -1;
	}
	osMutexRelease(i2cMutexHandle);
	return 0;
}

int8_t bsp_i2c_read(uint16_t addr , uint8_t* buff, uint8_t size)
{
	HAL_StatusTypeDef ret = HAL_OK;

	osMutexWait(i2cMutexHandle, osWaitForever);
	
	ret = HAL_I2C_Master_Receive(&hi2c1, addr, buff, size, 100);
	if(ret != HAL_OK)
	{
		ERR("[%s] error...%d\n", __func__, ret);

		ret = HAL_I2C_DeInit(&hi2c1);
		if(ret != HAL_OK)
		{
			ERR("HAL_I2C_DeInit error...%d\n", ret);
		}

		MX_I2C1_Init();

		ERR("i2c re-initialized\n");
		osMutexRelease(i2cMutexHandle);
		return -1;
	}
	osMutexRelease(i2cMutexHandle);
	return 0;
}


