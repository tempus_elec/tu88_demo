/**
  ******************************************************************************
  * File Name          : freertos.c
  * Description        : Code for freertos applications
  ******************************************************************************
  * This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * Copyright (c) 2018 STMicroelectronics International N.V. 
  * All rights reserved.
  *
  * Redistribution and use in source and binary forms, with or without 
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice, 
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other 
  *    contributors to this software may be used to endorse or promote products 
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this 
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under 
  *    this license is void and will automatically terminate your rights under 
  *    this license. 
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS" 
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT 
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT 
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "FreeRTOS.h"
#include "task.h"
#include "cmsis_os.h"
#include "stdbool.h"

/* USER CODE BEGIN Includes */     
#include "bsp.h"
#define Buff_size 7
#define TEMPUS_DEVICE_MAX_NUM 4
#define LED_ON 1
#define LED_OFF 0
/* USER CODE END Includes */

/* Variables -----------------------------------------------------------------*/
osThreadId defaultTaskHandle;

/* USER CODE BEGIN Variables */
static uint32_t count = 0;
static osMessageQId defaultQueHandle;

TU88_ST g_tu88 = {0};

/* USER CODE END Variables */

/* Function prototypes -------------------------------------------------------*/
void StartDefaultTask(void const * argument);

void MX_FREERTOS_Init(void); /* (MISRA C 2004 rule 8.1) */

/* USER CODE BEGIN FunctionPrototypes */
extern void StartCliTask(void const * argument);
uint32_t tu88_sensor[TEMPUS_DEVICE_MAX_NUM] = {0};
uint32_t tu88_LED_OFF_average[TEMPUS_DEVICE_MAX_NUM] = {0};
uint32_t tu88_LED_ON_average[TEMPUS_DEVICE_MAX_NUM] = {0};
uint32_t tu88_temp[TEMPUS_DEVICE_MAX_NUM] = {0};
uint32_t sensor_buff[TEMPUS_DEVICE_MAX_NUM*Buff_size];
bool LED_STATE=0;

int32_t tu88_0_offset	=	0;
int32_t tu88_1_offset	=	0;
int32_t tu88_2_offset	=	0;
int32_t tu88_3_offset	=	0;

uint32_t LED_OFF_STATE_COUNT=0,LED_ON_STATE_COUNT,SENS_TH=10000;//sensor threshold
/* USER CODE END FunctionPrototypes */

/* Hook prototypes */

/* Init FreeRTOS */

void MX_FREERTOS_Init(void) {
  /* USER CODE BEGIN Init */
       
  /* USER CODE END Init */

  /* USER CODE BEGIN RTOS_MUTEX */
  /* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* USER CODE BEGIN RTOS_SEMAPHORES */
  /* add semaphores, ... */
  /* USER CODE END RTOS_SEMAPHORES */

  /* USER CODE BEGIN RTOS_TIMERS */
  /* start timers, add new ones, ... */
  /* USER CODE END RTOS_TIMERS */

  /* Create the thread(s) */
  /* definition and creation of defaultTask */
  osThreadDef(defaultTask, StartDefaultTask, osPriorityNormal, 0, 256);
  defaultTaskHandle = osThreadCreate(osThread(defaultTask), NULL);

  /* USER CODE BEGIN RTOS_THREADS */
  /* add threads, ... */
  /* USER CODE END RTOS_THREADS */

  /* USER CODE BEGIN RTOS_QUEUES */
  /* add queues, ... */
  /* USER CODE END RTOS_QUEUES */
}

/* StartDefaultTask function */
void StartDefaultTask(void const * argument)
{

  /* USER CODE BEGIN StartDefaultTask */
	int8_t count=0,buff=1;
	int32_t tu88_buff[TEMPUS_DEVICE_MAX_NUM],temp_0,temp_1,temp_2,temp_3;
	bool flag[TEMPUS_DEVICE_MAX_NUM]={0};
	int32_t ave,i,j;
	uint32_t average(uint32_t *,int , int);	

	StartCliTask(NULL);
	LOG("\n\n\nTempus TU88 Running...\n");
	bsp_tu88_init();
  /* Infinite loop */
  for(;;)
  {
		if(g_tu88.monFlag)
		{
			//LOG("Run...%d\n", count++);
			bsp_tu88_read(tu88_sensor,tu88_temp);
		}
#if 0		
		for(j=0;j<TEMPUS_DEVICE_MAX_NUM;++j) sensor_buff[count*TEMPUS_DEVICE_MAX_NUM+j]=tu88_sensor[j];
		tu88_buff[0]=average(sensor_buff,0,buff) + tu88_0_offset;	//+50000;// tu88_sensor[0]tu88_sensor[0]+20000;//
		tu88_buff[1]=average(sensor_buff,1,buff) + tu88_1_offset;	//+50000;//tu88_sensor[1]tu88_sensor[1];//tu88_sensor[1]+20000;
		tu88_buff[2]=average(sensor_buff,2,buff) + tu88_2_offset;	//+600000;//tu88_sensor[2]tu88_sensor[2];//tu88_sensor[2]+600000;
		tu88_buff[3]=average(sensor_buff,3,buff) + tu88_3_offset;	//+600000;//tu88_sensor[3]tu88_sensor[3];//tu88_sensor[3]+600000;//
		
		ave=(tu88_buff[0]+tu88_buff[1]+tu88_buff[2]+tu88_buff[3])/4;
//		if(buff==Buff_size) tu88_0-=ave;tu88_1-=ave;tu88_2-=ave;tu88_3-=ave;
		if(count==Buff_size/2) {
			MSG("ROIC %ld %ld %ld %ld %ld %ld %ld %ld\n", tu88_buff[0], temp_0, tu88_buff[1], temp_1, tu88_buff[2], temp_2, tu88_buff[3], temp_3);
            if(LED_STATE==0){//LED_OFF
            	if(LED_OFF_STATE_COUNT>3){
            		tu88_LED_OFF_average[0]=tu88_LED_OFF_average[0]/2+tu88_buff[0]/2;
            		tu88_LED_OFF_average[1]=tu88_LED_OFF_average[1]/2+tu88_buff[1]/2;
            		tu88_LED_OFF_average[2]=tu88_LED_OFF_average[2]/2+tu88_buff[2]/2;
            		tu88_LED_OFF_average[3]=tu88_LED_OFF_average[3]/2+tu88_buff[3]/2;
            	}else{
            		tu88_LED_OFF_average[0]=tu88_buff[0];
            		tu88_LED_OFF_average[1]=tu88_buff[1];
            		tu88_LED_OFF_average[2]=tu88_buff[2];
            		tu88_LED_OFF_average[3]=tu88_buff[3];
            	};
            	++LED_OFF_STATE_COUNT;
            	LED_ON_STATE_COUNT=0;
            	//LED_ON_condition
               	for(i=0;i<TEMPUS_DEVICE_MAX_NUM;++i)
               		if(tu88_buff[i]>(tu88_LED_OFF_average[i]+SENS_TH)) flag[i]=1;
               	if((int)(flag[0]+flag[1]+flag[2]+flag[3])>0) LED_STATE=1;
            }else{//LED_ON
            	if(LED_ON_STATE_COUNT>3){
            		tu88_LED_ON_average[0]=tu88_LED_ON_average[0]/2+tu88_buff[0]/2;
            		tu88_LED_ON_average[1]=tu88_LED_ON_average[1]/2+tu88_buff[1]/2;
            		tu88_LED_ON_average[2]=tu88_LED_ON_average[2]/2+tu88_buff[2]/2;
            		tu88_LED_ON_average[3]=tu88_LED_ON_average[3]/2+tu88_buff[3]/2;
               	}else{
               		tu88_LED_ON_average[0]=tu88_buff[0];
             		tu88_LED_ON_average[1]=tu88_buff[1];
              		tu88_LED_ON_average[2]=tu88_buff[2];
               		tu88_LED_ON_average[3]=tu88_buff[3];
               	};
               	++LED_ON_STATE_COUNT;
               	LED_OFF_STATE_COUNT=0;
               	//LED off condition
               	for(i=0;i<TEMPUS_DEVICE_MAX_NUM;++i)
               		if(tu88_LED_ON_average[i]<(tu88_LED_OFF_average[i]+SENS_TH)) flag[i]=0;
               	if((int)(flag[0]+flag[1]+flag[2]+flag[3])==0) LED_STATE=0;
            };
            if(LED_STATE==1) MSG("LED ON\n");
            else MSG("LED OFF\n");
		}
		++count;
		++buff;
		if(count>(Buff_size-1)) count=0;
		if(buff>Buff_size) buff=Buff_size;
		bsp_led_green_tgl();
		osDelay(50);
#else
		osDelay(1000);
#endif
  }
  /* USER CODE END StartDefaultTask */
}

/* USER CODE BEGIN Application */
uint32_t average(uint32_t *sensor,int row, int buff){
   uint32_t sum=0,min=0,max=0;
   int i;
   max=sensor[(buff-1)*TEMPUS_DEVICE_MAX_NUM+row];
   min=sensor[(buff-1)*TEMPUS_DEVICE_MAX_NUM+row];
   for(i=0;i<buff;++i){
      sum+=sensor[i*TEMPUS_DEVICE_MAX_NUM+row];
      if(sensor[i*TEMPUS_DEVICE_MAX_NUM+row]>max) max=sensor[i*TEMPUS_DEVICE_MAX_NUM+row];
      if(sensor[i*TEMPUS_DEVICE_MAX_NUM+row]<min) min=sensor[i*TEMPUS_DEVICE_MAX_NUM+row];
   };
   if(buff<3)   return sum/buff;
   else return (sum-max-min)/(buff-2);
}
int8_t send_que_default(uint32_t msg, uint32_t timeout)
{
    osMessagePut (defaultQueHandle, msg, timeout);
    return 0;
}
/* USER CODE END Application */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
